import { LitElement, html, css} from 'lit-element';

class TestBootstrap extends LitElement {

    static get styles() {
        return css`
            .red { background-color: red}
            .green { background-color: green}
            .blue { background-color: blue}
        `;
    }

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
            <h2>TestBootstrap</h2>
            <div class="row">
                <div class="col red">1</div>
                <div class="col green">2</div>
                <div class="col blue">3</div>
            </div>
        `;
    }
}

customElements.define("test-bootstrap", TestBootstrap);