import { LitElement, html, css } from "lit-element";
import "../bootstrap-import/BootstrapImport";

class PersonHeader extends LitElement {

    render() {
        return html`
            <bootstrap-import></bootstrap-import>
            <nav class="navbar navbar-light bg-dark">
                <div class="container-fluid">
                    <a class="navbar-brand text-info" href="#">
                        <img src="resources/img/lit-logo.png" alt="" width="70" />
                        Base LitElement
                    </a>
                </div>
            </nav>
        `;
    }
}

customElements.define("person-header", PersonHeader);