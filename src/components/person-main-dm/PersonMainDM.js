import {LitElement, html} from 'lit-element';

class PersonMainDM extends LitElement {

    static get properties() {
        return {
            people: { type: Array }
        }
    }

    constructor() {
        super();

        this.fetchPeople()
    }

    fetchPeople() {
        setTimeout(() => {
            this.people = [
                { 
                    name: "Pedro Test", 
                    yearsInCompany: 2,
                    profile: "An awesome teacher I am",
                    photo: {
                        src: "./resources/img/yoda-troll.png",
                        alt: "yoda"
                    }
                },
                { 
                    name: "Another Test", 
                    yearsInCompany: 5,
                    profile: "This is fine",
                    photo: {
                        src: "./resources/img/this-is-fine.png",
                        alt: "ThisIsFine"
                    }
                },
                { 
                    name: "Moar Test",
                    yearsInCompany: 7,
                    profile: "STONKS",
                    photo: {
                        src: "./resources/img/stonks.png",
                        alt: "Stonks"
                    }
                },
                { 
                    name: "Jimmy \"The Quotes\" Test", 
                    yearsInCompany: 45,
                    profile: html`This is an <i>awesome</i> profile text using <b>lit-html template render</b> and <del>surprisingly</del> it works out of the box`,
                    photo: {
                        src: "./resources/img/doge.png",
                        alt: "Doge"
                    }
                }
            ];
        }, 5000);
    }

    updated() {
        this.dispatchEvent(new CustomEvent("fetch-people", {
            detail: {
                people: this.people
            }
        }));
    }


}

customElements.define('person-main-dm', PersonMainDM);