import { LitElement, html } from 'lit-element';

class PersonData extends LitElement {

    static get properties() {
        return {
            name: { type: String},
            yearsInCompany: { type: Number},
            _personInfo: { type: String }
        }
    }

    constructor() {
        super();
        this.name = "";
        this.yearsInCompany = 0;
        this._personInfo = "Junior";
    }

    updated(changedProps) {
        changedProps.forEach((old, prop) => {
            console.log(`Property: ${prop} changed from ${old}`)
        });

        if(changedProps.has("yearsInCompany")){
            if(this.yearsInCompany >= 7){
                this._personInfo = "Lead";
            }
            else if(this.yearsInCompany >= 5){
                this._personInfo = "Senior";
            }
            else if(this.yearsInCompany >= 3){
                this._personInfo = "Team";
            }
            else this._personInfo = "Junior";
        }
    }

    render() {
        return html`
            <div>
                <label>Nombre</label>
                <br>
                <input type="text" placeholder="Nombre" value="${this.name}" @input="${this.updateName}">
                <br>
                <label>Años en la empresa</label>
                <br>
                <input type="text" placeholder="Años en la empresa" value="${this.yearsInCompany}" @input="${this.updateYears}">
                <br>
                <input type="text" value="${this._personInfo}" readonly>
            </div>
        `;
    }

    updateName(e) {
        this.name = e.target.value;
    }

    updateYears(e) {
        this.yearsInCompany = e.target.value;
    }

}

customElements.define("person-data", PersonData);