import {LitElement, html} from 'lit-element';
import "../custom-event-emitter/CustomEventEmitter";
import "../event-receiver/EventReceiver";

class EventManager extends LitElement {
    static get properties() {
        return {
            
        }
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <h2>Event Manager</h2>
            <custom-event-emitter @test-event="${this.processEvent}"></custom-event-emitter>
            <event-receiver id="eventReceiver"></event-receiver>
        `;
    }

    processEvent({ detail }) {
        let eventReceiver = this.shadowRoot.getElementById("eventReceiver");

        eventReceiver.course = detail.course;
        eventReceiver.year = detail.year;
    }

}

customElements.define('event-manager', EventManager);