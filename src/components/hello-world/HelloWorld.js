import { LitElement, html } from 'lit-element';

class HelloWorld extends LitElement {
    render() {
        return html`
            <svg viewBox="500 500" width="200" height="100">
                <text x="50%" y="50%">Hello World!</text>
            </svg>
        `
    }
}

customElements.define("hello-world", HelloWorld);