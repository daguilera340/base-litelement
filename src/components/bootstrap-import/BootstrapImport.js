import { LitElement, html } from "lit-element";

class BootstrapImport extends LitElement {

    render() {
        return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
        `;
    }

    createRenderRoot() {
        return this;
    }
}

customElements.define("bootstrap-import", BootstrapImport);
