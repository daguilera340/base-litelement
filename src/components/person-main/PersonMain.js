import { LitElement, html } from "lit-element";
import "../person-data-list/PersonDataList";
import "../bootstrap-import/BootstrapImport";
import "../person-form/PersonForm";
import "../ripple-loader/RippleLoader";

class PersonMain extends LitElement {

    static get properties() {
        return {
            _people: {type: Array},
            isFormHidden: { type: Boolean},
            filters: { type: Object }
        };
    }

    constructor() {
        super();
        this.isFormHidden = true;
        this._people = [];
        this.filters = {};
    }

    get people() {
        return this._people || [];
    }

    set people(people) {
        this._people = people;
    }

    get _filteredPeople() {
        let filteredPeople = this.people;
        if(Object.keys(this.filters).length > 0){
            const { yearsInCompany } = this.filters;

            filteredPeople = this.people.filter(person => person.yearsInCompany <= yearsInCompany);
        }

        return filteredPeople || [];
    }

    render() {
        return html`
            <bootstrap-import></bootstrap-import>
            <h3 class="text-center">
                Personas
                <span class="fs-6">Total <b class="badge bg-success">${this.people.length}</b></span>
                <span class="fs-6">Mostrando <b class="badge bg-info">${this._filteredPeople.length}</b></span>
            </h3>
            <div class="row">
                <div class="row row-cols-2 row-cols-sm-4 ${this.isFormHidden ? "" : "d-none"}">
                ${this._filteredPeople.length > 0 
                ?   this._filteredPeople.map(
                    person => html`
                        <person-data-list 
                            name="${person.name}" 
                            yearsInCompany="${person.yearsInCompany}"
                            .profile="${person.profile}"
                            .photo="${person.photo}"
                            @delete-person="${this.deletePerson}"
                            @edit-person="${this.editPerson}"
                        ></person-data-list>
                    `
                )
                : html`
                        <ripple-loader class="m-auto"></ripple-loader>
                    `
                }
                </div>
                <person-form 
                    id="personForm"
                    class="${this.isFormHidden ? "d-none" : ""} border rounded"
                    @close-person-form="${this.closePersonForm}"
                    @save-person-form="${this.savePersonForm}"
                ></person-form>
            </div>
        `;
    }

    updated(changedProps) {
        if(changedProps.has("_people")) {
            this.dispatchEvent(new CustomEvent("updated-people",{
                detail: {
                    people: this.people
                }
            }));
        }
    }

    deletePerson({ detail }) {
       let { name } = detail;

       this.people = this.people.filter(person => person.name !== name);
    }

    closePersonForm() {
        this.isFormHidden = true;
        this.shadowRoot.querySelector("#personForm").person = { name: "", yearsInCompany: "", profile: "" };
    }

    savePersonForm({ detail }) {
        const { person } = detail;

        let existingPersonIndex = this.people.findIndex(p => p.name === person.name);

        if(existingPersonIndex !== -1){
            let existingPerson = this.people[existingPersonIndex];
            this.people[existingPersonIndex] = { ...existingPerson, ...person };
            this.people = [...this.people];
        }
        else {
            this.people = [person, ...this.people];
        }

        this.closePersonForm();
    }

    editPerson({ detail }) {
        const { name } = detail;
        const person = this.people.find(p => p.name === name);
        const personData = { ...person };

        this.shadowRoot.querySelector("#personForm").person = personData;
        this.isFormHidden = false;
    }
}

customElements.define("person-main", PersonMain);