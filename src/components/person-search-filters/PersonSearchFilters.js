import {LitElement, html, css} from 'lit-element';
import "../bootstrap-import/BootstrapImport";

class PersonSearchFilters extends LitElement {
    static get properties() {
        return {
            minRange: { type: Number },
            maxRange: { type: Number },
            rangeStep: { type: Number },
            rangeValue: { type: Number }
        }
    }

    static get styles() {
        return css`
            .range-wrapper{
                display: flex;
                align-items: center;
                justify-content: space-evenly;
            }
        `;
    }

    constructor() {
        super();

        this.minRange = 0;
        this.maxRange = 9999;
        this.rangeStep = 1;
        this.rangeValue = 0;
    }

    render() {
        return html`
        <bootstrap-import></bootstrap-import>
            <h3>Filtrar</h3>
            <label class="form-label">Años en la empresa</label><br>
            <div class="range-wrapper">
                <span>${this.minRange}</span>
                <input 
                    type="range"
                    class="form-range w-75 d-inline-block mx-2" 
                    min="${this.minRange}" 
                    max="${this.maxRange}" 
                    step="${this.rangeStep}"
                    @change="${this.updateRangeValue}"
                    @input="${this.realTimeRangeIndicator}"
                    .value="${this.rangeValue}"
                >
                <span>${this.maxRange}</span>
                <output class="badge bg-primary ms-2">${this.rangeValue}</output>
            </div>
        `;
    }

    updateRangeValue(e) {
        let currentValue = e.target.value;
        this.rangeValue = currentValue;

        this.dispatchEvent(new CustomEvent("update-person-filters", {
            bubbles: true,
            composed: true,
            detail: {
                filters: {
                    yearsInCompany: currentValue
                }
            }
        }));
    }

    realTimeRangeIndicator(e) {
        this.rangeValue = e.target.value;
    }

}

customElements.define('person-search-filters', PersonSearchFilters);