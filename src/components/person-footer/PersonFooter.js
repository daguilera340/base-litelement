import { LitElement, html, css} from "lit-element";
import "../bootstrap-import/BootstrapImport";

class PersonFooter extends LitElement {

    static get styles() {
        return css`
            :host {
                width: 100%;
                position: fixed;
                bottom: 0;
                left: 0;
            }
        `;
    }

    render() {
        return html`
            <bootstrap-import></bootstrap-import>
            <footer class="mt-auto py-3 bg-light">
                <div class="container-fluid">
                    <span class="text-muted">PersonApp© 2021</span>
                </div>
            </footer>
        `;
    }
}

customElements.define("person-footer", PersonFooter);