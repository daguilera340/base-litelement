import {LitElement, html} from 'lit-element';

class PersonStats extends LitElement {
    static get properties() {
        return {
            people: { type: Array }
        }
    }

    constructor() {
        super();

        this.people = [];
    }

    updated(changedProps) {
        if(changedProps.has("people")){
            this.updatePeopleStats();
        }
    }

    updatePeopleStats() {
        let peopleStats = {
            numberOfPeople: this.people.length
        };

        this.dispatchEvent(new CustomEvent("update-people-stats", {
            detail: { peopleStats }
        }));
    }

}

customElements.define('person-stats', PersonStats);