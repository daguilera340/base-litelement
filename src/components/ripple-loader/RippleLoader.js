import {LitElement, html, css} from 'lit-element';
import "../bootstrap-import/BootstrapImport";

class RippleLoader extends LitElement {
    static get properties() {
        return {

        }
    }

    constructor() {
        super();
    }

    static get styles() {
        return css`
                :host{
                    width: 100%;
                    display: flex;
                    justify-content: center

                }
                .lds-ripple {
                    display: inline-block;
                    position: relative;
                    width: 170px;
                    height: 170px;
                }
                .lds-ripple div {
                    position: absolute;
                    border: 4px solid #89c7e6;
                    opacity: 1;
                    border-radius: 50%;
                    animation: lds-ripple 1s cubic-bezier(0, 0.2, 0.8, 1) infinite;
                }
                .lds-ripple div:nth-child(2) {
                    animation-delay: -0.5s;
                    }
                    @keyframes lds-ripple {
                    0% {
                        top: 100px;
                        left: 100px;
                        width: 0;
                        height: 0;
                        opacity: 1;
                    }
                    100% {
                        top: 0px;
                        left: 0px;
                        width: 200px;
                        height: 200px;
                        opacity: 0;
                    }
                }

            `
    }

    render() {
        return html`
            <bootstrap-import></bootstrap-import>
            <div class="lds-ripple">
                <div></div>
                <div></div>
            </div>
        `;
    }

}

customElements.define('ripple-loader', RippleLoader);