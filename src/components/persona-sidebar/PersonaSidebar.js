import { LitElement, html } from "lit-element";
import  "../bootstrap-import/BootstrapImport";
import "../person-search-filters/PersonSearchFilters";

class PersonaSidebar extends LitElement {

    static get properties() {
        return {
            peopleStats: { type: Object }
        }
    }

    render() {
        return html`
            <bootstrap-import></bootstrap-import>
            <aside class="ps-3">
                <section class="d-flex flex-column align-items-center">
                    <button class="btn btn-success w-50 mt-5" @click="${this.showPersonForm}">
                        <svg class="bi" width="32" height="32" fill="currentColor">
                            <use xlink:href="resources/svg/bootstrap-icons.svg#person-plus-fill"/>
                        </svg>
                    </button>
                    <div class="mt-5">
                        <person-search-filters maxRange="50"></person-search-filters>
                    </div>
                </section>
            </aside>
        `;
    }

    showPersonForm() {
        this.dispatchEvent(new CustomEvent("show-person-form", {
            composed: true,
            bubbles: true,
        }));
    }
}

customElements.define("persona-sidebar", PersonaSidebar);