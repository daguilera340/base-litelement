import { LitElement, html} from 'lit-element';
import "../bootstrap-import/BootstrapImport";

class PersonDataList extends LitElement {

    static get properties() {
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            photo: {type: Object},
            profile: {type: String}
        };
    }

    constructor() {
        super();

        this.name = "";
        this.yearsInCompany = 0;
    }
    
    render() {
        return html`
            <bootstrap-import></bootstrap-import>
            <div class="card h-100" style="width: 18rem;">
                ${this.photo 
                    ? html`<img src="${this.photo.src}" alt="${this.photo.alt}" width="90">`
                    : html`<svg class="bi" width="90" height="90" fill="currentColor">
                                <use xlink:href="resources/svg/bootstrap-icons.svg#emoji-dizzy-fill"/>
                            </svg>
                    `
                }
                <div class="card-body">
                    <h5 class="card-title">${this.name}</h5>
                    <p class="card-text">${this.profile}</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">
                            ${this.yearsInCompany} años en la empresa
                        </li>
                    </ul>
                </div>
                <div class="card-footer">
                    <button class="btn btn-danger" @click="${this.deletePerson}">
                        <svg class="bi" width="32" height="32" fill="currentColor">
                            <use xlink:href="resources/svg/bootstrap-icons.svg#trash"/>
                        </svg>
                    </button>
                    <button class="btn btn-primary" @click="${this.editPerson}">
                        <svg class="bi" width="32" height="32" fill="currentColor">
                            <use xlink:href="resources/svg/bootstrap-icons.svg#pencil-square"/>
                        </svg>
                    </button>
                </div>
            </div>
        `;
    }

    deletePerson() {
        this.dispatchEvent(new CustomEvent(
            "delete-person",
            {
                detail: {
                    name: this.name
                }
            }
        ));
    }

    editPerson() {
        this.dispatchEvent(new CustomEvent(
            "edit-person", 
            {
                detail: {
                    name: this.name
                }    
            }
        ));
    }
}

customElements.define("person-data-list", PersonDataList);