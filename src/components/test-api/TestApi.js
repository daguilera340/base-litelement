import {LitElement, html} from 'lit-element';


class TestApi extends LitElement {
    static get properties() {
        return {
            films: { type: Array }
        }
    }

    constructor() {
        super();
        this.films = [];
    }

    firstUpdated() {
        this.getFilms();
    }

    render() {
        return html`
            <h4>Star Wars Movies</h4>
            ${this.films.map(film => html`
                <p>
                    ${film.title} directed by ${film.director}
                </p>
            `)}
        `;
    }

    async getFilms() {
        const result = await fetch("https://swapi.dev/api/films");

        if(result.ok){
            const jsonResult = await result.json();
            this.films = jsonResult.results;
        }
        else {
            throw new Error(`HTTP ERR: ${result.status}`);
        }
    }

}

customElements.define('test-api', TestApi);