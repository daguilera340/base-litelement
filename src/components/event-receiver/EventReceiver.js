import {LitElement, html} from 'lit-element';


class EventReceiver extends LitElement {
    static get properties() {
        return {
            course: { type: String},
            year: { type: String}
        }
    }

    constructor() {
        super();
    }

    render() {
        return html`
            <h3>Receptor Evento</h3>
            <p>Curso: ${this.course}</p>
            <p>Año: ${this.year}</p>
        `;
    }    

}

customElements.define('event-receiver', EventReceiver);