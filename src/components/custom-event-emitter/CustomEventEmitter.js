import { LitElement, html} from "lit-element";
import "../bootstrap-import/BootstrapImport";

class CustomEventEmitter extends LitElement {

    static get properties() {
        return {};
    }

    constructor() {
        super();
    }

    render() {
        return html `
            <bootstrap-import></bootstrap-import>
            <button class="btn btn-primary" @click="${this.sendEvent}"> Click me to send things </button>
        `;
    }

    sendEvent(e) {
        this.dispatchEvent(
            new CustomEvent("test-event", {
                detail: {
                    course: "Tech-U!",
                    year: 2021
                }
            })
        );
    }
}

customElements.define("custom-event-emitter", CustomEventEmitter);