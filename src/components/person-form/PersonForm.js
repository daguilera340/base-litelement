import { LitElement, html } from "lit-element";
import { ifDefined } from "lit-html/directives/if-defined";
import "../bootstrap-import/BootstrapImport";

class PersonForm extends LitElement {

    static get properties() {
        return {
            person: { type: Object}
        }
    }

    constructor() {
        super();
        this.person = {};
    }

    render() {
        return html`
            <bootstrap-import></bootstrap-import>
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre</label>
                        <input 
                            type="text" 
                            id="name" 
                            class="form-control" 
                            placeholder="Nombre" 
                            .value="${ifDefined(this.person.name)}"
                            @input="${this.updatePersonData}"
                        >
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea 
                            id="profile" 
                            class="form-control" 
                            placeholder="Perfil"
                            @input="${this.updatePersonData}"
                            .value="${ifDefined(this.person.profile)}"
                        ></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input 
                            type="number"
                            id="yearsInCompany" 
                            min="0" 
                            class="form-control" 
                            placeholder="Años en la empresa" 
                            .value="${ifDefined(this.person.yearsInCompany)}"
                            @input="${this.updatePersonData}"
                        >
                    </div>
                    <div class="m-2">
                        <button type="button" class="btn btn-primary" @click="${this.closePersonForm}">
                            Back 
                            <svg class="bi" width="32" height="32" fill="currentColor">
                                <use xlink:href="resources/svg/bootstrap-icons.svg#arrow-return-left"/>
                            </svg>
                        </button>
                        <button type="button" class="btn btn-success" @click="${this.savePerson}">
                            Save
                            <svg class="bi" width="32" height="32" fill="currentColor">
                                <use xlink:href="resources/svg/bootstrap-icons.svg#save"/>
                            </svg>
                        </button>
                    </div>
                </form>
            </div>
        `;
    }

    closePersonForm() {
        this.dispatchEvent(new CustomEvent("close-person-form", {
            composed: true,
            bubbles: true
        }));
    }

    updatePersonData(e) {
        const property = e.target.id;

        this.person[property] = e.target.value;
    }

    savePerson() {
        this.dispatchEvent(new CustomEvent("save-person-form", {
            composed: true,
            bubbles: true,
            detail: {
                person: { ...this.person }
            }
        }));
    }
}

customElements.define("person-form", PersonForm);