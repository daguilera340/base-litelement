import { LitElement, html} from "lit-element";
import "../components/person-header/PersonHeader";
import "../components/person-main/PersonMain";
import "../components/person-footer/PersonFooter";
import "../components/persona-sidebar/PersonaSidebar";
import "../components/bootstrap-import/BootstrapImport";
import "../components/person-stats/PersonStats";
import "../components/person-main-dm/PersonMainDM";

class PersonApp extends LitElement {

    render() {
        return html`
            <bootstrap-import></bootstrap-import>
            <person-stats 
                id="personStats"
                @update-people-stats="${this.updatedPeopleStats}"
            ></person-stats>
            <person-main-dm @fetch-people="${this.fetchPeople}"></person-main-dm>
            <person-header></person-header>
            <div class="row">
                <div class="col-2">
                    <persona-sidebar
                        id="sidebar" 
                        @show-person-form="${this.showPersonForm}"
                        @update-person-filters="${this.updateFilters}"
                    ></persona-sidebar>
                </div>
                <div class="col-10">
                    <person-main 
                        id="main"
                        @updated-people="${this.updatedPeople}"
                    ></person-main>
                </div>
            </div>
            <person-footer></person-footer>
        `;
    }

    showPersonForm() {
        this.shadowRoot.querySelector("#main").isFormHidden = false;
    }

    updatedPeople({ detail }) {
        this.shadowRoot.querySelector("#personStats").people = detail.people;
    }

    updatedPeopleStats({ detail }) {
        this.shadowRoot.querySelector("#sidebar").peopleStats = detail.peopleStats;
    }

    updateFilters({ detail }) {
        const { filters } = detail;
        this.shadowRoot.querySelector("#main").filters = {...filters}
    }

    fetchPeople({ detail }) {
        this.shadowRoot.querySelector("#main").people = detail.people;
    }
}

customElements.define("person-app", PersonApp);